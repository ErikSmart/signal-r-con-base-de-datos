﻿const connection = new signalR.HubConnectionBuilder()
  .withUrl("/chatHub")
  .build();

var urlParams = new URLSearchParams(window.location.search);
const group = urlParams.get("group") || "Chat_Home";
document.getElementById("titulo-sala").innerText = group;

const li = document.getElementById("messagesList");
li.innerHTML = "";
//Consigue los datos
fetch("http://localhost:5001/Datos/dato")
  .then(response => response.json())
  .then(data => {
    for (let index = 0; index < data.length; index++) {
      const element = data[index].nombre;
      console.log(element);
      li.innerHTML += `<br> ${element}`;
    }
  })
  .catch(error => console.error(error));

connection.on("ReceiveMessage", (user, message) => {
  li.innerHTML = "";
  /*  const msg = message
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;"); */
  const fecha = new Date().toLocaleTimeString();
  const mensajeAMostrar = message;
  var cambiar = JSON.parse(message);
  cambiar.forEach(element => {
    li.innerHTML += `<br> ${element}`;
  });
  //document.getElementById("messagesList").appendChild(li);
});

connection
  .start()
  .then(() => {
    // Este bloque de código se ejecuta cuando se establece la conexión con el servidor
    connection
      .invoke("AddToGroup", group)
      .catch(err => console.error(err.toString()));
  })
  .catch(err => {
    console.error(err.toString());
  });

document.getElementById("sendButton").addEventListener("click", event => {
  // 1 = conectado
  if (connection.connection.connectionState !== 1) {
    alert("usted no está conectado con el servicio");
    return;
  }

  const user = document.getElementById("userInput").value;
  const message = document.getElementById("messageInput").value;
  connection
    .invoke("SendMessage", user, message, group)
    .catch(err => console.error(err.toString()));
  event.preventDefault();
});
