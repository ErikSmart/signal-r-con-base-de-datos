using System;
using System.Linq;
using HolaMundoSignalR.Data;
using HolaMundoSignalR.Hubs;
using HolaMundoSignalR.Services;
using Microsoft.AspNetCore.Mvc;

namespace DatosController.Controllers
{
    public class DatosController : Controller
    {
        private readonly ApplicationDbContext context;

        public DatosController(ApplicationDbContext context)
        {
            this.context = context;
        }
        [HttpGet]
        public IActionResult dato()
        {
            return Ok(context.Personas.Select(x => new { x.Nombre }));
        }
    }
}