﻿using HolaMundoSignalR.Data;
using HolaMundoSignalR.Hubs;
using HolaMundoSignalR.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HolaMundoSignalR.Services
{
    public interface IDatabaseChangeNotificationService
    {
        void Config();
    }

    public class SqlDependencyService : IDatabaseChangeNotificationService
    {
        private SqlConnection conn;


        public void Conectar()
        {
            string connString = configuration.GetConnectionString("DefaultConnection");
            var conn = new SqlConnection(connString);
            var comando = new SqlCommand(@"SELECT Nombre FROM [dbo].Personas", conn);

        }

        private readonly IConfiguration configuration;
        private readonly IHubContext<ChatHub> chatHub;
        private readonly ApplicationDbContext context;

        public SqlDependencyService(IConfiguration configuration,
            IHubContext<ChatHub> chatHub)
        {
            this.configuration = configuration;
            this.chatHub = chatHub;

        }

        public void Config()
        {
            SuscribirseALosCambiosDeLaTablaPersonas();
        }

        public List<Persona> SuscribirseALosCambiosDeLaTablaPersonas()
        {

            string connString = configuration.GetConnectionString("DefaultConnection");
            var conn = new SqlConnection(connString);

            conn.Open();
            //No funciona si usas Select *
            var cmd = new SqlCommand(@"SELECT Nombre FROM [dbo].Personas", conn);

            cmd.Notification = null;
            SqlDependency dependency = new SqlDependency(cmd);

            dependency.OnChange += new OnChangeEventHandler(Personas_Cambio);
            SqlDependency.Start(connString);
            var dataReader = cmd.ExecuteReader(); // Hay que correr el query
            var Lista = dataReader.Cast<IDataRecord>().Select(x => new { Nombre = (string)x["Nombre"] }).ToList();


            //No funciona si usas Select *
            List<Persona> personas = new List<Persona>();
            while (dataReader.Read())
            {
                Persona teacher = new Persona();
                teacher.Id = Convert.ToInt32(dataReader["Id"]);
                teacher.Nombre = Convert.ToString(dataReader["Nombre"]);
                personas.Add(teacher);
            }
            conn.Close();

            return personas;

        }
        public void Personas_Cambio(object sender, SqlNotificationEventArgs e)
        {

            if (e.Type == SqlNotificationType.Change)
            {
                var mensaje = ObtenerMensajeAMostrar(e);
                List<Persona> personas = new List<Persona>();

                var json = JsonConvert.SerializeObject(new[] { sender });
                var sacar = json.Select(x => new { x });


                chatHub.Clients.All.SendAsync("ReceiveMessage", json, mensaje);
            }

            SuscribirseALosCambiosDeLaTablaPersonas(); // Importante: Hay que volver a suscribirse

        }

        private string ObtenerMensajeAMostrar(SqlNotificationEventArgs e)
        {

            string connString = configuration.GetConnectionString("DefaultConnection");
            var conn = new SqlConnection(connString);

            //No funciona si usas Select *
            conn.Open();
            List<Persona> personas = new List<Persona>();

            string sql = "Select * From personas";
            SqlCommand command = new SqlCommand(sql, conn);
            SqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                Persona teacher = new Persona();
                teacher.Id = Convert.ToInt32(dataReader["Id"]);
                teacher.Nombre = Convert.ToString(dataReader["Nombre"]);
                personas.Add(teacher);
            }
            conn.Close();
            var solo = personas.Select(x => x.Nombre);
            var json = JsonConvert.SerializeObject(personas.ToArray());
            var jsona = JsonConvert.SerializeObject(solo, Formatting.None);




            return jsona.ToString();







            /*   switch (e.Info)
              {
                  case SqlNotificationInfo.Insert:
                      return "Un registro se inserto";
                  case SqlNotificationInfo.Delete:
                      return "Un registro ha sido borrado";
                  case SqlNotificationInfo.Update:
                      return "Se actualizo";
                  default:
                      return "Un cambio desconocido ha ocurrido";
              } */


        }



    }


}
