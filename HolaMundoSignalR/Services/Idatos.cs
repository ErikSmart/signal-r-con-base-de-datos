using System.Collections.Generic;
using HolaMundoSignalR.Models;

namespace HolaMundoSignalR.Services
{
    public interface idatos
    {
        List<Persona> SuscribirseALosCambiosDeLaTablaPersonas();
    }
}